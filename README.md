# MINSAIT

[Eduardo Muñoz](https://www.linkedin.com/in/eduardo-mu%C3%B1oz-lorenzo-14144a144/)

### Install Libraries
```
pip3 install pandas
pip3 install numpy
pip3 install matplotlib
pip3 install xlrd
pip3 install -U scikit-learn
pip3 install xgboost
pip3 install seaborn
```

### Folders
1. [Data](https://gitlab.com/emunozlorenzo/minsait/tree/master/data)
2. [Notebooks](https://gitlab.com/emunozlorenzo/minsait/tree/master/notebooks) 

	2.1 Fast Approach

	2.2 Cleaning Data

	2.3 Tuning Model

	2.4 EDA

3. [Submission](https://gitlab.com/emunozlorenzo/minsait/tree/master/submission) Submission File
4. [Modeldata](https://gitlab.com/emunozlorenzo/minsait/tree/master/modeldata) Data Preprocessed
5. [Images](https://gitlab.com/emunozlorenzo/minsait/tree/master/images)
